package com.felipeforbeck.vacuum.application.impl;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class rest_temp {
	
	public static void main(String args[]){
	      try{
			FileInputStream fstream = new FileInputStream("/Users/shreyamoonka/Documents/log");
			DataInputStream in = new DataInputStream(fstream);
	        BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			while ((strLine = br.readLine()) != null) 	{
				if (strLine.startsWith("swagger_url")){
					String[] l=strLine.split("url:");
					String url=l[1];
					String uri= "http://localhost:8090/v1/microservices";
					RestTemplate restTemplate= new RestTemplate();
					HttpHeaders headers=new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					Map<String,String> value= new HashMap<>();
					value.put("swagger_url", url);
				    ResponseEntity<String> result = restTemplate.postForEntity(uri, value, String.class);
				    String res= result.getBody();
				    System.out.println(res);
				    }
			
			else if (strLine.startsWith("origin_host")){
				String[] l=strLine.split(":");
				String ori_host=l[1];
				strLine=br.readLine();
				String[] m=strLine.split(":");
				String method=m[1];
				strLine=br.readLine();
				String[] n=strLine.split(":");
				String tar_host=n[1];
				strLine=br.readLine();
				String[] r=strLine.split(":");
				String tar_path=r[1];
				String uri= "http://localhost:8090/v1/microservices";
				RestTemplate restTemplate= new RestTemplate();
				HttpHeaders headers=new HttpHeaders();
				headers.setContentType(MediaType.APPLICATION_JSON);
				Map<String,String> value= new HashMap<>();
				value.put("origin_host", ori_host);
				value.put("method", method);
				value.put("target_host", tar_host);
				value.put("target_path", tar_path);
				HttpEntity<Map<String, String>> entity= new HttpEntity<>(value, headers);
		        ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
			    String res= result.getBody();
			    System.out.println(res);
			    }
		}
			in.close();
	      }catch(Exception e){
				System.err.println("Error: " + e.getMessage());
	      }
	}

}
