package com.felipeforbeck.vacuum.application.impl;

import java.io.*;


class log_read {
   public static void main(String args[]){
      try{
		FileInputStream fstream = new FileInputStream("/Users/shreyamoonka/Documents/log");
		DataInputStream in = new DataInputStream(fstream);
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String strLine;
		
		while ((strLine = br.readLine()) != null) 	{
			if (strLine.startsWith("swagger_url")){
				String[] l=strLine.split("url:");
				String url=l[1];
				
				String command= "curl -XPOST http://localhost:8090/v1/microservices -H 'Content-Type: application/json' -d '{\"swagger_url\":"+ url+"}'";
				System.out.println(command);
				Runtime run = Runtime.getRuntime();
				Process p = run.exec(command);
				p.waitFor();
//				ProcessBuilder ps = new ProcessBuilder(new String[]{ "curl", "http://localhost:8090/v1/microservices", "-H","Content-Type:application/json", "-d", "'{\"swagger_url\":", url,"}'" });
//			    ps.redirectErrorStream(true);
//			    Process p = ps.start();  
			    
				InputStream stdout= p.getInputStream();
				BufferedReader reader = new BufferedReader(new InputStreamReader(stdout)); 
			    StringBuilder sb_swag = new StringBuilder(); 
			    String line = ""; 
			    while ((line = reader.readLine()) != null) { 
			        sb_swag.append(line + "\n"); 
			        System.out.println(line);
			    }
//			    p.waitFor();
			}
			else if(strLine.startsWith("origin_host")){
				String[] l=strLine.split(":");
				String ori_host=l[1];
				strLine=br.readLine();
				String[] m=strLine.split(":");
				String method=m[1];
				strLine=br.readLine();
				String[] n=strLine.split(":");
				String tar_host=n[1];
				strLine=br.readLine();
				String[] r=strLine.split(":");
				String tar_path=r[1];
				String command= "curl -XPOST http://localhost:8090/v1/requests -H 'Content-Type: application/json' -d '{\"origin_host\":" +ori_host+ ",\"method\":"+ method+",\"target_host\":"+tar_host+",\"target_path\":"+tar_path+"}'"; 
				System.out.println(command);
				Runtime run= Runtime.getRuntime();
				Process pr=run.exec(command);
				pr.waitFor();
				
//				ProcessBuilder ps = new ProcessBuilder(new String[]{ "curl", "http://localhost:8090/v1/requests", "-H","'Content-Type:application/json'", "-d", "'{\"origin_host\":" ,ori_host, "\"method\":", method,"\"target_host\":",tar_host,"\"target_path\":",tar_path,"}'"}); 
//			    ps.redirectErrorStream(true);
//			    Process pr = ps.start();  
			    
				InputStream std=pr.getInputStream();
				BufferedReader reader = new BufferedReader(new InputStreamReader(std)); 
			    StringBuilder sb = new StringBuilder(); 
			    String line = ""; 
			    while ((line = reader.readLine()) != null) { 
			        sb.append(line + "\n"); 
			        System.out.println(line);

			    }
//			    pr.waitFor();
			}
		}
		in.close();
		}catch (Exception e){
			System.err.println("Error: " + e.getMessage());
		}
	}
}
